package io.nutz.maven.deploy.bean;

import java.util.Date;

import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Name;
import org.nutz.dao.entity.annotation.Table;

@Table("t_deploy_job")
public class DeployJob {

    @Id
    private int id;
    @Name
    private String name;

    /**
     * 访问秘钥
     */
    @Column("tk")
    private String token;

    /**
     * 工作目录
     */
    @Column("cwd")
    @ColDefine(width=1024)
    private String cwd;

    /**
     * 执行的命令
     */
    @Column("cmds")
    @ColDefine(width=1024)
    private String cmds;

    /**
     * 谁创建的,预留
     */
    @Column("ow")
    private String owner;
    
    /**
     * 创建时间
     */
    @Column("ct")
    private Date createTime;
    
    /**
     * 使用启用
     */
    @Column("eb")
    private boolean enable;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCwd() {
        return cwd;
    }

    public void setCwd(String cwd) {
        this.cwd = cwd;
    }

    public String getCmds() {
        return cmds;
    }

    public void setCmds(String cmds) {
        this.cmds = cmds;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}
