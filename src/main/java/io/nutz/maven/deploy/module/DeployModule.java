package io.nutz.maven.deploy.module;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.nutz.dao.Dao;
import org.nutz.ioc.impl.PropertiesProxy;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Streams;
import org.nutz.lang.Strings;
import org.nutz.lang.random.R;
import org.nutz.lang.util.NutMap;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;

import io.nutz.maven.deploy.bean.DeployJob;

@At("/deploy")
@IocBean(create="init", depose="depose")
public class DeployModule {
    
    private static final Log log = Logs.get();
    
    protected ExecutorService es;
    
    @Inject
    protected Dao dao;
    
    @Inject
    protected PropertiesProxy conf;
    
    @At
    @Ok("json:full")
    public NutMap trigger(String name, String token) {
        if (Strings.isBlank(name)) {
            return new NutMap("err", "name is blank");
        }
        if (Strings.isBlank(token)) {
            return new NutMap("err", "token is blank");
        }
        DeployJob job = dao.fetch(DeployJob.class, name);
        if (job == null) {
            return new NutMap("err", "no such job name");
        }
        if (!job.isEnable()) {
            return new NutMap("err", "such job is disabled");
        }
        String id = R.UU32();
        es.submit(new Runnable() {
            public void run() {
                try {
                    List<String> envs = new ArrayList<>();
                    for (Map.Entry<String, String> en : System.getenv().entrySet()) {
                        envs.add(en.getKey() + "=" + en.getValue());
                    };
                    conf.keys().forEach((key)->{
                        if (key.startsWith("ENV.")) {
                            envs.add(key.substring(4) + "=" + conf.get(key));
                        }
                    });
                    String[] env = envs.toArray(new String[envs.size()]);
                    Process p = Runtime.getRuntime().exec(job.getCmds().split(" "), env, new File(job.getCwd()));
                    es.submit(()-> {
                        try {
                            Streams.writeAndClose(new FileOutputStream("/tmp/" + id + ".stdout"), p.getInputStream());
                        }
                        catch (FileNotFoundException e) {
                            log.debug("EOF?", e);
                        }
                    });
                    es.submit(()-> {
                        try {
                            Streams.writeAndClose(new FileOutputStream("/tmp/" + id + ".stderr"), p.getErrorStream());
                        }
                        catch (FileNotFoundException e) {
                            log.debug("EOF?", e);
                        }
                    });
                    p.getOutputStream().close();
                    p.waitFor();
                    log.infof("job(id=%s) done", id);
                }
                catch (Throwable e) {
                    log.error(e.getMessage(), e);
                }
            }
        });
        return new NutMap("id", id);
    }
    
    @At("/log/?/?")
    @Ok("raw")
    public File readLog(String id, String type) {
        return new File("/tmp/" + id + "." + type);
    }
    
    public void init() {
        es = Executors.newCachedThreadPool();
    }
    public void depose() {
        es.shutdown();
    }

}
